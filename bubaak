#!/usr/bin/env python3

from os.path import abspath, dirname
from select import POLLIN, POLLHUP
from sys import setswitchinterval, argv, exit as sys_exit
from time import clock_gettime, CLOCK_REALTIME

from bbk.cmdline import parse_arguments, setup_debugging
from bbk.compiler import Compiler, CompilationUnit
from bbk.dbg import print_stdout, warn, dbgv
from bbk.env import init_env, get_env, change_to_env, cleanup_env
from bbk.properties import get_properties, PropertiesList
from bbk.result import Result
from bbk.tools.klee import Klee
from bbk.tools.slowbeast import SlowBeast
from bbk.tools.toolsmgr import ToolsManager
from bbk.utils import err
from bbk.version import get_version
from svcomp.helpers import (
    parse_svcomp_prps,
    result_to_sv_comp,
    generate_witness,
    parse_yml_input,
    SVCompProperty,
)


def read_fd(fd, partial_lines, tool, stream):
    if stream == "stdout":
        fl = tool.proc().stdout
    elif stream == "stderr":
        fl = tool.proc().stderr
    else:
        raise RuntimeError(f"Unknown stream: {stream}")

    assert fd == fl.fileno()

    line = fl.readline()
    if line:
        ln = line.decode("utf-8", "ignore")
        if ln[-1] not in ("\n", "\r"):
            partial_lines.setdefault(fl, []).append(line)
            return False
        elif fl in partial_lines:
            assert "\n" in ln
            line = (b"".join(partial_lines[fl])) + line
            ln = line.decode("utf-8")
            del partial_lines[fl]
        if __debug__:
            isstderr = stream == "stderr"
            header = f"{tool.name()}/{2 if isstderr else 1}"
            dbgv(
                f"##[{header:20}] '\033[31m{ln}\033[0m'",
                print_ws="",
                color="gray" if isstderr else "reset",
            )
        tool.parser().parse(line, stream)
        return True
    return False


def get_slowbeast_args(args, properties, options=None):
    options = options or []
    for prp in properties:
        if not prp.is_unreach():
            if prp.is_no_signed_overflow():
                options.extend(("-check", "no-overflow"))
            else:
                options.extend(("-check", prp.key()))
        if prp.is_termination():
            if "-bself" in options:
                return None  # BSELF does not support termination

    if "-bself" in options:
        options.append("-forbid-floats")
        options.append("-forbid-threads")

    if args.sv_comp:
        # ["-pointer-bitwidth", str(args.pointer_bitwidth)]
        # options += ["-svcomp-witness", "-se-exit-on-error", "-se-replay-errors"]
        # FIXME: we do not replay errors for now to find bugs
        options += ["-svcomp-witness", "-exit-on-error", "-replay-error"]
    return options


def get_klee_args(args, properties):
    options = []
    if args.sv_comp:
        options.append("-write-witness")
    # if args.timeout and args.timeout > 0:
    #    options.append(f"-max-time={args.timeout}")
    def add(p):
        if p not in options:
            options.append(p)

    no_memleak = True
    for prp in properties:
        if prp.is_unreach():
            add("-exit-on-error-type=Assert")
        elif prp.is_memcleanup():
            # NOTE: this elif must be before memleak, because that one matches too
            add("-check-memcleanup")
            add("-exit-on-error-type=Leak")
        elif prp.is_no_memleak():
            no_memleak = False
            add("-check-leaks")
            add("-exit-on-error-type=Leak")
        elif prp.is_valid_deref():
            add("-exit-on-error-type=Ptr")
            add("-exit-on-error-type=ReadOnly")
            add("-exit-on-error-type=BadVectorAccess")
        elif prp.is_valid_free():
            add("-exit-on-error-type=Free")
        elif prp.is_no_signed_overflow():
            add("-ubsan-runtime")
            add("-exit-on-error-type=Overflow")
            add("-exit-on-error-type=ReportError")
        elif prp.is_termination():
            pass
        elif isinstance(prp, SVCompProperty) and prp.is_memcleanup():
            add("-check-memcleanup")
        else:
            return None
    if no_memleak:
        # these leak memory, so do not use them if we look for leaks
        # FIXME: do not add these if libc or POSIX are not used by the program
        add("-libc=klee")
        # FIXME: we do not use POSIX runtime since syscall is not modelled by KLEE
        # and thus it is symbolic, which basically breaks the POSIX runtime of KLEE
        # add("-libc=uclibc")
        # add("-posix-runtime")
    else:
        add("-libc=klee")
    if args.exec_witness:
        add("-write-harness")
    return options


def main_loop(tools):
    def final_results(results):
        return any((r.is_correct() or r.is_incorrect() for r in results))

    poller = tools.poller()
    # FIXME: do an object from this
    partial_lines = {}

    while poller:
        poll_fds = poller.poll(timeout=1000)
        for fd, ev in poll_fds:
            if ev & POLLIN:
                read_fd(fd, partial_lines, *poller.get_data(fd))
            if ev == POLLHUP:
                # Tool finishes/crash... Read all what is left on
                # this fd and the other fd of the tool
                tool, _ = poller.get_data(fd)
                assert fd in tool.fds()
                for n, tool_fd in enumerate(tool.fds()):
                    tmp_stream = "stdout" if n == 0 else "stderr"
                    while read_fd(tool_fd, partial_lines, tool, tmp_stream):
                        pass
                # remove fds from poller
                tools.stop_tool(tool)

                tool.parser().finish(tool.proc().wait())
                results = tool.parser().result()
                if results:
                    if final_results(results):
                        # we've got a result, stop all tools and
                        # return the result
                        tools.stop_tools()
                        return results, tool
                    print_stdout("----------------")
                    print_stdout(f"{tool.name()} finished with the following results:")
                    for r in results:
                        r.describe()
                break

        # check timeout
        current_time = clock_gettime(CLOCK_REALTIME)
        for tool, timeout in tools.get_timeouted_tools(current_time):
            print_stdout(f"Tool '{tool.name()}' reached timeout {timeout}s")
            tools.stop_tool(tool)
            if not tools.has_running():
                # return timeout as the last tool reached timeout
                # this is mainly for debugging, without this, we just return 'unknown'
                return [Result(Result.TIMEOUT, prp=None)], None
    return None, None


def initialize_env():
    # we do not use threads, at least not know...
    setswitchinterval(100)

    init_env(argv[0])
    change_to_env()


def get_programs(args):
    programs = []
    for prog in args.prog:
        if prog.endswith(".yml"):
            yaml_spec = parse_yml_input(prog)
            if yaml_spec is None:
                raise RuntimeError(f"Failed parsing {prog}")
            files = yaml_spec["input_files"]  # it is one file right now...
            path = f"{(abspath(dirname(prog)))}/{files}"
            if int(yaml_spec["options"]["data_model"][-2:]) != args.pointer_bitwidth:
                warn(
                    "YAML input file has different architecture: "
                    f"{yaml_spec['options']['data_model']} != ILP{args.pointer_bitwidth}"
                )
            programs.append(CompilationUnit(path, yaml_spec["options"]["language"]))
        elif prog.endswith(".c") or prog.endswith(".i"):
            programs.append(CompilationUnit(prog, lang="C"))
        elif prog.endswith(".bc") or prog.endswith(".ll"):
            programs.append(CompilationUnit(prog, lang="llvm"))
        else:
            raise RuntimeError(f"Unsupported input file: {prog}")

    return programs


def main():
    args = parse_arguments()
    setup_debugging(args)

    print_stdout(f"Bubaak version {get_version()}")

    initialize_env()

    properties = parse_properties(args)

    bitcode, bitcode_san, warnings = compile_programs(args, properties)
    if warnings:
        overflow = properties.get("no-signed-overflow")
        if overflow and any(
            ("warning: overflow in expression;" in line for line in warnings)
        ):
            # This is a work-around for the problem that the translation to LLVM looses
            # some information -- in this case, an overflow is detected by clang, reported,
            # but LLVM is generated without this overflow
            process_results(
                [Result(Result.INCORRECT, overflow, "Signed overflow detected")],
                properties,
                None,
                args,
            )
            return

    tools = ToolsManager()

    timeout = None
    if args.timeout is not None and args.timeout > 0:
        timeout = args.timeout

    klee_options = get_klee_args(args, properties)
    if klee_options is not None and (args.tool is None or "klee" in args.tool):
        tools.add_to_batch(
            Klee(properties, klee_options, timeout=timeout), [bitcode_san or bitcode]
        )

    slowbeast_options = get_slowbeast_args(args, properties)
    if slowbeast_options is not None and (
        args.tool is None or "slowbeast" in args.tool
    ):
        tools.add_to_batch(
            SlowBeast(properties, args=slowbeast_options, timeout=timeout), [bitcode]
        )

    slowbeast_options = get_slowbeast_args(args, properties, ["-bself"])
    if slowbeast_options is not None and (
        args.tool is None or "slowbeast-bself" in args.tool
    ):
        tools.add_to_batch(
            SlowBeast(
                properties,
                args=slowbeast_options,
                name="slowbeast-bself",
                timeout=timeout,
            ),
            [bitcode],
        )

    try:
        run_tools(args, properties, tools)
    except Exception as e:
        warn("Caught exception, killing tools and cleaning up...")
        raise e
    finally:
        # make sure all tools were killed
        tools.stop_tools()
        tools.kill_tools()
        tools.kill_stopped_tools()
        if not args.save_files:
            tools.cleanup()
            cleanup_env()
        else:
            dbgv(f"Not cleaning up files on demand, working dir: {get_env().workdir}")


def compile_programs(args, properties):
    cc = Compiler(properties)
    cc.add_include_dirs(*(args.I if args.I else ()))

    programs = get_programs(args)
    use_ubsan = any((p.is_no_signed_overflow() for p in properties))
    use_asan = any((p.is_valid_deref() for p in properties))
    bitcode_san = None
    if use_ubsan or use_asan:
        bitcode_san, warnings = cc.compile_with_sanitizers(
            programs, use_asan, use_ubsan
        )
        if bitcode_san is None:
            err("Failed compiling code with sanitizers")
        assert bitcode_san.endswith(".bc"), bitcode_san
        new_name = f"{bitcode_san[:-3]}.sanitized.bc"
        bitcode_san = cc.link_llvm([bitcode_san], outp=new_name)
    # compile the bitcode also without sanitizers for Slowbeast
    # addargs = ["-O1"] if all((prp.is_unreach() for prp in properties)) else []
    bitcode, warnings = cc.compile(programs)
    return bitcode, bitcode_san, warnings


def parse_properties(args):
    if args.sv_comp:
        properties = get_svcomp_properties(args)
    else:
        properties = get_properties(args)
    if not properties:
        warn("No properties given!")
        sys_exit(1)
    print_stdout("Checking the following:", color="white")
    for prp in properties:
        print_stdout(f"  - {prp.descr()}", color="white")
    return properties


def get_svcomp_properties(args):
    # get properties from YAML files
    # FIXME: each set of properties should be attached to a compilation unit,
    #  not to the whole program....
    properties = PropertiesList()
    for prog in args.prog:
        yaml_spec = None
        if prog.endswith(".yml"):
            yaml_spec = parse_yml_input(prog)
            if yaml_spec is None:
                raise RuntimeError(f"Failed parsing {prog}")

        codedirpath = f"{(abspath(dirname(prog)))}"
        properties.extend(parse_svcomp_prps(args, get_env(), codedirpath, yaml_spec))
    return properties


def run_tools(args, properties, tools):
    tools.start_batch()
    results, tool = None, None
    try:
        results, tool = main_loop(tools)
    except KeyboardInterrupt:
        print_stdout("Interrupted, exitting...")
        tools.stop_tools()
        tools.kill_tools()
        sys_exit(0)

    process_results(results, properties, tool, args)


def process_results(results, properties, tool, args):
    if not results:
        warn("did not get any result")
        if args.sv_comp:
            print_stdout(("----------------"))
            print_stdout("SV-COMP verdict: unknown", color="white")
    else:
        print_stdout(("----------------"))
        if tool:
            print_stdout(f"Tool '{tool.name()}' got a result", color="blue")
        for result in results:
            result.describe()
        if args.sv_comp:
            print_stdout(("----------------"))
            if len(results) == 0:
                print_stdout("SV-COMP verdict: unknown(no results)", color="white")
                return
            if len(results) != 1:
                if (
                    all((r.is_correct() for r in results))
                    or all(
                        (
                            r.is_incorrect() and r.prp() == results[0].prp()
                            for r in results
                        )
                    )
                    or all(
                        (r.is_incorrect() and r.prp().is_memsafety() for r in results)
                    )
                ):
                    dbgv("Multiple violations of the same (meta)property")
                else:
                    print_stdout(
                        "SV-COMP verdict: unknown(multiple results)", color="white"
                    )
                    return

            svcomp_result = result_to_sv_comp(results, properties)
            print_stdout(f"SV-COMP verdict: {svcomp_result}", color="white")
            if svcomp_result.startswith("true") or svcomp_result.startswith("false"):
                print_stdout(f"Generating witness: {args.sv_comp_witness}")
                generate_witness(tool.resultsdir() if tool else ".", args, results)


if __name__ == "__main__":
    main()
