#include <map>

#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "ValueFlow.h"

llvm::raw_ostream& operator<<(llvm::raw_ostream& os, const vf::Pointer& p) {
    os << "Pointer[" << &p << "]";
    return os;
}

llvm::raw_ostream& operator<<(llvm::raw_ostream& os, const vf::Value& v) {
    os << "Value[" << &v << "]";
    return os;
}

namespace vf {

void Value::dump() const { llvm::errs() << *this << "\n"; }
void Pointer::dump() const { llvm::errs() << *this << "\n"; }

std::string Value::toString() const { return "Value["+ std::to_string(reinterpret_cast<uint64_t>(this)) +"]"; }
std::string Pointer::toString() const { return "Pointer["+ std::to_string(reinterpret_cast<uint64_t>(this)) +"]"; }

void Object::dump() const {
    llvm::errs() << "Object[" << name << "]{";
    for (auto& it : pointer_edges) {
        it.first->dump();
        llvm::errs() << "--> " << it.second->getName() << "\n";
    }
    llvm::errs() << "}\n";
}

void Get::dump() const {
    llvm::errs() << "Get(" << pointer << ", ";
    for (auto *idx : indices) {
        llvm::errs() << *idx << ", ";
    }
    llvm::errs() << ")\n";
}

std::string Get::toString() const {
    std::string ret = "Get(" + pointer.toString() + ",";
    for (auto *idx : indices) {
        ret += idx->toString() + ", ";
    }
    ret += ")";
    return ret;
}

std::unique_ptr<Operation> GEPtoOp(const llvm::GetElementPtrInst& FEP) {
    Pointer pointer;
    std::vector<Value *> indices;
    return std::make_unique<Get>(pointer, indices);
}

std::unique_ptr<Operation> Operation::get(const llvm::Instruction& I) {
    using namespace llvm;

    switch (I.getOpcode()) {
      case Instruction::GetElementPtr: return GEPtoOp(*cast<GetElementPtrInst>(&I));
      default: return nullptr;
    }

    abort();
}

} // namespace vf




namespace {
using namespace llvm;

struct ValueFlow : public ModulePass {
  static char ID;
  ValueFlow() : ModulePass(ID) {}

  void getAnalysisUsage(AnalysisUsage &Info) const override {
      Info.setPreservesAll();
  }

  bool runOnModule(Module &M) override {
      for (auto& F: M) {
          runOnFunction(F);
      }
    return false;
  }

  void runOnFunction(Function& F) {
      if (F.isDeclaration())
          return;
     errs() << "Processing: ";
     errs().write_escaped(F.getName()) << '\n';

     for (auto& BB: F) {
        summarizeBasicBlock(BB);
     }
  }

  void summarizeBasicBlock(BasicBlock& block);
};

void ValueFlow::summarizeBasicBlock(BasicBlock& block) {
    for (auto& I : block) {
        auto op = vf::Operation::get(I);
        if (!op)
            continue;

        errs() << "OP: ";
        op->dump();
    }

}

}  // namespace

char ValueFlow::ID = 0;
static RegisterPass<ValueFlow> VFP("bbk-valueflow",
                                   "Bubaak ValueFlow Pass",
                                   true /* Only looks at CFG */,
                                   true /* Analysis Pass */);

static RegisterStandardPasses VFSP(
    PassManagerBuilder::EP_FullLinkTimeOptimizationLast,
    [](const PassManagerBuilder &Builder,
       legacy::PassManagerBase &PM) { PM.add(new ValueFlow()); });
