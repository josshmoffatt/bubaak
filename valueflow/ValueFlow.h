#pragma once

#include <map>
#include <vector>
#include <string>
#include <memory>

namespace llvm {
  class Value;
  class Instruction;
}

namespace vf {

class Value {
    llvm::Value *llvmValue;
public:
    virtual void dump() const;
    virtual std::string toString() const;
};

class Object {
    using EdgesMapTy = std::map<Value *, Object *>;
    EdgesMapTy pointer_edges;
    const std::string name;

public:
    Object(const std::string& name) : name(name) {}
    const std::string& getName() const { return name; }

    virtual void dump() const;
};

class Pointer : public Value {
public:
    void dump() const override;
    virtual std::string toString() const override;
};

class Operation {
public:
    virtual void dump() const = 0;
    virtual std::string toString() const = 0;
    static std::unique_ptr<Operation> get(const llvm::Instruction&);
};

class Get : public Operation {
    const Pointer pointer;
    const std::vector<Value *> indices;

public:
    Get(Pointer& pointer, std::vector<Value *>& indices)
        : pointer(std::move(pointer)), indices(std::move(indices)) {}
    void dump() const override;
    std::string toString() const override;
};

} // namespace vf

