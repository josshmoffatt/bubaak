# from tempfile import mkdtemp
from shutil import rmtree
from os import environ

from bbk.env import get_env
from bbk.result import Result
from bbk.tools.tool import ToolOutputParser, Tool


def line_contains(line, *args):
    return any((a in line for a in args))


class SlowBeastParser(ToolOutputParser):
    def __init__(self, properties, args):
        self._properties = properties
        self._no_error_found = False
        self._killed_paths = []
        self._errors = []
        self._retval = None
        self._args = args

    def is_bse(self):
        return "-bself" in self._args or "-bse" in self._args

    def try_get_prp(self, key):
        for p in self._properties:
            if p.key() == key:
                return p
        return None

    def get_prp(self, key):
        prp = self.try_get_prp(key)
        if prp is None:
            raise RuntimeError(f"Did not find property: {key}")
        return prp

    def add_error_or_killed(self, prpkey, line):
        prp = self.get_prp(prpkey)
        if prp:
            self._errors.append(Result(Result.INCORRECT, prp, line))
        else:
            self._killed_paths.append(Result(Result.UNKNOWN, None, line))

    def _parse_stdout(self, line):
        if line.startswith("Found errors:"):
            if line == "Found errors: 0":
                self._no_error_found = True
        if line.startswith("Killed paths:"):
            num = int(line.split()[2])
            if num == 0:
                # BSELF does not report the paths correctly right now
                pass  # assert len(self._killed_paths) == 0
            elif num > 0 and len(self._killed_paths) == 0:
                # slowbeast killed a path, but we didn't catch the warning
                # (maybe there was not one). Add at least this line
                self._killed_paths.append(line)
        if "KILLED STATE: " in line:
            self._killed_paths.append(line)

    # if "[assertion error]" in line and "reachable." in line:
    #    # BSELF and BSE report that a location is reachable, but do not
    #    # differentiate between what happend
    #    prp = self.try_get_prp("unreach")
    #    if prp is None:
    #        prp = self.try_get_prp("no-signed-overflow")
    #    if prp is None:
    #        prp = "<unknown>"
    #    self.add_error_or_killed(prp.key(), line)

    def _parse_stderr(self, line):
        if "[assertion error]: error function called!" in line:
            self.add_error_or_killed("unreach", line)
        elif "[non-termination]: an infinite execution found" in line:
            self.add_error_or_killed("termination", line)
        elif "[assertion error]:" in line and line_contains(
            line, "signed integer overflow", "signed integer underflow"
        ):
            self.add_error_or_killed("no-signed-overflow", line)
        elif "[memory error] - uninitialized read" in line:
            self._killed_paths.append(line)
        elif self.is_bse() and "Ignoring function pointer call: " in line:
            self._killed_paths.append(line)

    def finish(self, retcode):
        self._retval = retcode

    def result(self):
        if self._errors:
            assert not self._no_error_found
            return self._errors
        if self._retval is None:
            return None
        if self._retval == 0:
            if self._killed_paths:
                return [Result(Result.UNKNOWN, None, kp) for kp in self._killed_paths]
            if not self._no_error_found:
                return [Result(Result.ERROR, None, f"inconsistency")]
            return [Result(Result.CORRECT, prp, "") for prp in self._properties]
        return [Result(Result.ERROR, None, f"retval: {self._retval}")]


class SlowBeast(Tool):
    def __init__(
        self, properties, args=None, parser=None, name="slowbeast", timeout=None
    ):
        the_args = args.copy()
        for prp in properties:
            if prp.is_unreach():
                for fn in prp.error_funs():
                    the_args.extend(["-error-fn", fn])

        new_env = environ.copy()
        new_env["PYTHONOPTIMIZE"] = "1"

        super().__init__(
            f"{get_env().srcdir}/slowbeast/sb",
            the_args,
            parser or SlowBeastParser(properties, args),
            name,
            envir=new_env,
            timeout=timeout,
        )
        self._out_dir = f"{get_env().workdir}/{name}-out"  # mkdtemp(prefix="sb-out.")

    def resultsdir(self):
        return self._out_dir

    def cmd(self, progs, add_options=None):
        return (
            [self._exe, "-out-dir", self._out_dir]
            + self._args
            + (add_options or [])
            + progs
        )
