from signal import SIGINT

from bbk.poller import Poller


class ToolsManager:
    def __init__(self, batch=None):
        self._tools = set()
        self._stopped_tools = []
        self._poller = Poller()
        self._start_batch = batch or []

    def __len__(self):
        return len(self._tools)

    def __iter__(self):
        return self._tools.__iter__()

    def poller(self):
        return self._poller

    def add_to_batch(self, tool, progs=None, args=None):
        self._start_batch.append((tool, progs, args))

    def has_running(self):
        return len(self._tools) > 0

    def start_batch(self):
        for tool, progs, args in self._start_batch:
            self.start_tool(tool, progs, args)
        self._start_batch = []

    def start_tool(self, tool, progs=None, args=None):
        tool.start(progs, args)
        self._tools.add(tool)
        self._poller.add_tool(tool)

    def stop_tool(self, tool):
        """
        Send SIGINT to the tool and stop watching its fds.
        """
        assert tool in self._tools
        for fd in tool.fds():
            self._poller.remove_fd(fd)
        tool.proc().send_signal(SIGINT)
        self._tools.remove(tool)
        self._stopped_tools.append(tool)

    def kill_tool(self, tool):
        """
        Kill the tool. It is assumed that stop_tool() was called first,
        that is that fds have been already removed from the poller.
        """
        tool.proc().terminate()
        tool.proc().kill()

    # def pause_tool(self, tool):
    #    assert tool in self._tools
    #    tool.proc().send_signal(SIGSTOP)

    def stop_tools(self, exclude=None):
        """Stop all tools"""
        for tool in (t for t in self._tools.copy() if t not in (exclude or ())):
            self.stop_tool(tool)

    def kill_tools(self, exclude=None):
        """Kill all tools"""
        for tool in (t for t in self._tools if t not in (exclude or ())):
            self.kill_tool(tool)

    def kill_stopped_tools(self, exclude=None):
        """Kill all tools"""
        for tool in (t for t in self._stopped_tools if t not in (exclude or ())):
            self.kill_tool(tool)

    def get_timeouted_tools(self, current_time):
        ret = []
        for tool in self._tools:
            if tool.timeouted(current_time):
                ret.append((tool, tool.timeout()))
        return ret

    def cleanup(self):
        for t in self._stopped_tools:
            t.cleanup()
        # clean-up also still running tools if there are some
        # -- but assert that there should not be any
        for t in self._tools:
            self.kill_tool(t)
            t.cleanup()
        assert not self._tools, "Still have running tools"
