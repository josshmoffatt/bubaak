from bbk.env import get_env
from bbk.utils import _popen
from time import clock_gettime, CLOCK_REALTIME


class ToolOutputParser:
    def parse(self, line: bytes, stream: str):
        """
        Called for each line of the tool's output.
        @param stream is either "stdout" or "stderr"
        """
        line_s = line.decode("utf-8", "ignore").strip()
        if stream == "stdout":
            self._parse_stdout(line_s)
        elif stream == "stderr":
            self._parse_stderr(line_s)
        else:
            raise RuntimeError(f"Unknown stream: {stream}")

    def _parse_stdout(self, _):
        pass

    def _parse_stderr(self, _):
        pass

    def finish(self, retcode):
        """Called when the tool finishes"""
        raise NotImplementedError()

    def result(self):
        """Return result (anytime during parsing) or None if there is none"""
        raise NotImplementedError()


class Tool:
    def __init__(
        self, exe, args=None, parser=None, name=None, envir=None, timeout=None
    ):
        assert parser, "Need a parser"
        assert name, "Need a name"
        self._name = name
        self._env = get_env()
        self._exe = exe
        self._args = args or []
        self._environ = envir
        self._parser = parser
        self._proc = None
        self._start_time = None
        self._timeout = timeout

    def name(self):
        return self._name

    def exe(self):
        return self._exe

    def args(self):
        return self._args

    def env(self):
        return self._env

    def proc(self):
        return self._proc

    def parser(self):
        return self._parser

    def resultsdir(self):
        return None

    def start_time(self):
        return self._start_time

    def timeout(self):
        return self._timeout

    def timeouted(self, current_time, to_scale=1.0):
        if self._timeout is None or self._start_time is None:
            return False
        return (current_time - self._start_time) > to_scale * self._timeout

    def fds(self):
        """Get stdout and stderr (in this order) filedescriptors"""
        assert self.proc(), "Process is not running"
        return self.proc().stdout.fileno(), self.proc().stderr.fileno()

    def cmd(self, progs, add_options=None):
        return [self._exe] + self._args + (add_options or []) + progs

    def start(self, progs, add_options=None):
        cmd = self.cmd(progs, add_options)
        start_time = clock_gettime(CLOCK_REALTIME)
        print(
            f"## Run {self.name()} [time: {start_time - self.env().start_time}] with timeout {self.timeout()}"
        )
        print("#", " ".join(cmd))
        self._proc = _popen(cmd, self._environ)
        self._start_time = start_time
        return self._proc

    def cleanup(self):
        pass
