from bbk.env import get_env
from bbk.result import Result
from bbk.tools.tool import ToolOutputParser, Tool
from bbk.dbg import dbg
from os import environ
from os.path import exists


def line_contains(line, *args):
    return any((a in line for a in args))


class KleeParser(ToolOutputParser):
    def __init__(self, properties):
        self._properties = properties
        self._errors = []
        self._killed_paths = []
        self._retval = None
        self._memsafety = any(
            (
                prp.is_valid_deref() or prp.is_valid_free() or prp.is_no_memleak()
                for prp in properties
            )
        )
        self._only_termination = len(properties) == 1 and next(iter(properties)).is_termination()

    def get_prp(self, key):
        for p in self._properties:
            if p.key() == key:
                return p
        return None

    def add_error_or_killed(self, prpkey, line):
        prp = self.get_prp(prpkey)
        if prp:
            dbg("KLEE found an error")
            self._errors.append(Result(Result.INCORRECT, prp, line))
        else:
            dbg("KLEE found a different error")
            self._killed_paths.append(Result(Result.UNKNOWN, None, line))

    def _parse_stderr(self, line):
        if "ASSERTION FAIL:" in line:
            if self._only_termination:
                # ignore ASSERTION errors as those just terminate a path
                return
            self.add_error_or_killed("unreach", line)
            # else we're not looking for unreach call
        elif line_contains(
            line,
            "silently concretizing (reason: floating point)",
            "Call to pthread_create",
            "unsupported pthread API.",
            "failed external call: ",
            ": divide by zero",
            "KLEE: WARNING: Maximum stack size reached.",
            "WARNING ONCE: skipping fork (memory cap exceeded)",
            "return void when caller expected a result"
        ) or ("WARNING: killing" in line and "over memory cap: " in line):
            self._killed_paths.append(Result(Result.UNKNOWN, None, line))
        elif ": memory error:" in line:
            if self._memsafety:
                if "memory error: memory leak detected" in line:
                    self.add_error_or_killed("no-memleak", line)
                elif line_contains(
                    line,
                    "memory error: out of bound pointer",
                    "memory error: object read only",
                    "memory error: calling nullptr"
                ):
                    self.add_error_or_killed("valid-deref", line)
                elif line_contains(
                    line,
                    "memory error: invalid pointer: free",
                    "memory error: free of alloca",
                    "memory error: free of global",
                ):
                    self.add_error_or_killed("valid-free", line)
                else:
                    self._killed_paths.append(Result(Result.UNKNOWN, None, line))

            elif "memory error: memory not cleaned up" in line:
                self.add_error_or_killed("memcleanup", line)
            else:
                self._killed_paths.append(Result(Result.UNKNOWN, None, line))
        elif "KLEE: ERROR:" in line and\
                line_contains(line,
                              "signed-integer-overflow",
                              "integer division overflow",
                              ": overshift error",
                              "shift out of bounds"):
            self.add_error_or_killed("no-signed-overflow", line)
        elif self._memsafety and line_contains(line, "KLEE: WARNING: Failed resolving segment in memcleanup check"):
            self._killed_paths.append(Result(Result.UNKNOWN, None, line))

    def finish(self, retcode):
        self._retval = retcode

    def result(self):
        if self._errors:
            return self._errors
        if self._retval is None:
            return None
        if self._killed_paths:
            return self._killed_paths
        if self._retval == 0:
            # if self._killed_paths:
            #    return [Result(Result.UNKNOWN, kp) for kp in self._killed_paths]
            # assert self._no_error_found
            return [Result(Result.CORRECT, prp, "") for prp in self._properties]
        return [Result(Result.ERROR, None, f"retval: {self._retval}")]


class Klee(Tool):
    def __init__(self, properties, args=None, parser=None, name="klee", timeout=None):
        the_args = [
            "-dump-states-on-halt=0",
            "--output-stats=0",
            "--use-call-paths=0",
            # "--optimize=false",
            "-silent-klee-assume=1",
            "-istats-write-interval=60s",
            "-timer-interval=10",
            "-only-output-states-covering-new=1",
            "-use-forked-solver=0",
            "-external-calls=pure",
            "-max-memory=8000",
            "-output-source=false",
            "-malloc-symbolic-contents",
        ] + (args or [])

        error_fns = []
        for prp in properties:
            if prp.is_unreach():
                error_fns += prp.error_funs()
        if error_fns:
            the_args.extend(["-error-fn", ",".join(error_fns)])

        new_env = environ.copy()
        new_env[
            "LD_LIBRARY_PATH"
        ] = f"{get_env().srcdir}/klee/build/lib/:{environ['LD_LIBRARY_PATH']}"
        klee_runtime_dir = f"{get_env().srcdir}/klee/build/lib/klee/runtime"
        if exists(klee_runtime_dir):
            # If this dir exists, we're out of build and we must tell KLEE
            # that there it finds its libraries.
            # Otherwise KLEE will get them from the build automatically
            # and we do not need to set anything.
            new_env["KLEE_RUNTIME_LIBRARY_PATH"] = klee_runtime_dir

        super().__init__(
            f"{get_env().srcdir}/klee/build/bin/klee",
            the_args,
            parser or KleeParser(properties),
            name,
            envir=new_env,
            timeout=timeout
        )

    def resultsdir(self):
        return f"{self._env.workdir}/klee-last"
