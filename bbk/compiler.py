from os.path import join as pathjoin, basename

from bbk.dbg import dbg, print_stderr
from bbk.env import get_env
from bbk.utils import err, _popen


class CompilationUnit:
    def __init__(self, path, lang="C"):
        self.path = path
        self.lang = lang

    def __repr__(self):
        return f"CompilationUnit({self.path}, lang={self.lang})"


def _run(cmd):
    dbg(f"# {' '.join(cmd)}")
    proc = _popen(cmd)
    sout, serr = proc.communicate()
    if proc.returncode != 0:
        dbg(f"# command returned {proc.returncode}")
        if sout:
            print_stderr("stdout:")
            print_stderr(sout.decode("utf-8"), color="red")
        if serr:
            print_stderr("stderr:")
            print_stderr(serr.decode("utf-8"), color="red")
        err(f"command returned {proc.returncode}")
    dbg(sout.decode("utf-8") if sout else "")
    dbg(serr.decode("utf-8") if serr else "")

    return sout, serr


class Compiler:
    def __init__(self, properties):
        self._env = get_env()
        self._cflags = []
        self._cppflags = []
        self._properties = properties
        self._ubsan_flags = ["-fsanitize=signed-integer-overflow", "-fsanitize=shift"]
        self._asan_flags = ["-Xclang", "-fsanitize-address-use-after-scope"]

    def cflags_append(self, flag):
        self._cflags.append(flag)

    def cppflags_append(self, flag):
        self._cflags.append(flag)

    def add_include_dirs(self, *args):
        for d in args:
            if d.startswith("-I"):
                self.cppflags_append(d)
            else:
                self.cppflags_append(f"-I{d}")

    def compile(self, cus, outd=None, addargs=None, parse_warnings=None):
        llvm_files = []
        warnings = []
        for cu in cus:
            if cu.lang == "llvm":
                llvm_files.append(cu.path)
            elif cu.lang == "C":
                bc, tmp = self.compile_c(
                    cu,
                    addargs=addargs,
                    warnings=parse_warnings or ["warning: overflow in expression;"],
                )
                llvm_files.append(bc)
                warnings.extend(tmp)
            else:
                raise RuntimeError(f"Unsupported unit to compile: {cu}")

        assert len(llvm_files) > 0, "No input files"
        if len(llvm_files) > 1:
            bitcode = self.link_llvm(llvm_files, outd=outd)
        else:
            bitcode = llvm_files[0]

        return bitcode, warnings

    def compile_with_sanitizers(self, cus, asan, ubsan, outd=None, addargs=None):
        addargs = addargs or []
        if asan:
            addargs.extend(self._asan_flags)
        if ubsan:
            addargs.extend(self._ubsan_flags)
        return self.compile(cus, outd, addargs)

    def compile_c(self, cu: CompilationUnit, outp=None, addargs=None, warnings=None):
        assert cu.lang == "C"
        dbg(f"## compiling {cu}")

        path = cu.path
        if outp is None:
            outp = pathjoin(self._env.workdir, basename(path) + ".bc")

        cflags = [
            "-D__inline=",
            "-fgnu89-inline",
            "-Xclang",
            "-disable-O0-optnone",
            "-fno-vectorize",
            "-fno-slp-vectorize",
            "-finline-functions",
        ] + self._cflags
        cppflags = self._cppflags

        cmd = (
            ["clang", "-emit-llvm", "-c", "-g"]
            + cflags
            + cppflags
            + ["-o", outp, path]
            + (addargs or [])
        )
        sout, serr = _run(cmd)

        dbg(f"## compiled to {outp}")

        bad_lines = []
        if not warnings:
            return outp, []

        for line in map(str, serr.splitlines()):
            for fail in warnings:
                if fail in line:
                    bad_lines.append(line)

        return outp, bad_lines

    def link_llvm(self, paths, outp=None, outd=None):
        dbg(f"## linking {paths}")

        if outd is None:
            outd = self._env.workdir
        if outp is None:
            outp = pathjoin(outd, "code.bc")

        cmd = ["llvm-link", "-o", outp] + paths
        _run(cmd)
        dbg(f"## linked files to {outp}")

        return outp

    def opt(self, path, opts=None, outp=None, outd="/tmp/"):
        dbg(f"Optimizing {path}")

        if outp is None:
            outp = pathjoin(outd, "optcode.bc")

        cmd = ["opt", "-o", outp, path] + (opts or [])
        _run(cmd)

        dbg(f"Optimized files to {outp}")

        return outp
