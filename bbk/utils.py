from subprocess import Popen, PIPE
from .dbg import print_stderr


def err(msg):
    print_stderr(msg, color="RED")
    exit(1)


def _popen(cmd, env=None):
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, env=env)
    return p
