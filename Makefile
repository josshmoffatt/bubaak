all: mypy pylint

mypy:
	mypy bubaak bbk/

pylint:
	pylint bubaak bbk/

fixme:
	pylint --enable=fixme bubaak bbk/

# FORMATTING
black:
	black bubaak bbk/ --exclude slowbeast --exclude klee

autopep:
	autopep8 --in-place --aggressive --aggressive --recursive bubaak bbk

_build-and-copy: slowbeast-package build-klee
	mkdir -p build
	cd build && git init && git clean -xdf
	mkdir -p build/klee/build build/slowbeast
	cp -r slowbeast/dist/sb/* build/slowbeast
	cp -r klee/build/install/* build/klee/build
	cp -r bbk/ svcomp/ build/
	cp bubaak build/
	cp LICENSE* README.md build/
	git rev-parse HEAD > build/git-version.txt

dist: _build-and-copy
	cd build && find . -name '*.so' | xargs strip
	cd build && find . -name '*.so.*' | xargs strip
	cd build && rm -rf bbk/__pycache__ bbk/tools/__pycache__
	cd build && git add bubaak LICENSE* README.md svcomp/*.py bbk/
	cd build && git add git-version.txt
	cd build/slowbeast && git add sb libpython* base_library.zip lib-dynload/ llvmlite/ numpy/ z3/
	cd build/klee/build && strip bin/klee
	cd build/klee/build && git add bin/klee bin/ktest-tool lib/
	cd build && git commit -a --allow-empty -m 'Update build' && git clean -xdf

slowbeast-llvmlite:
	test -d slowbeast/llvmlite || (cd slowbeast && git clone https://github.com/mchalupa/llvmlite)
	cd slowbeast/llvmlite && python3 ./setup.py build

slowbeast-package: slowbeast-llvmlite
	make pyinstaller -C slowbeast


build-klee-uclibc:
	test -d klee/klee-uclibc || (cd klee && git clone https://github.com/klee/klee-uclibc)
	test -f klee/klee-uclibc/config.log || (cd klee/klee-uclibc && ./configure --make-llvm-lib)
	make -C klee/klee-uclibc -j4 KLEE_CFLAGS="-DKLEE_SYM_PRINTF"

build-klee: build-klee-uclibc
	mkdir -p klee/build
	test -f klee/build/CMakeCache.txt ||\
		(cd klee/build && cmake .. -DCMAKE_INSTALL_PREFIX=./install -DENABLE_POSIX_RUNTIME=ON -DKLEE_UCLIBC_PATH=../klee-uclibc)
	make -j4 -C klee/build
	make install -C klee/build

setup: slowbeast-llvmlite build-klee
	pip install z3-solver --upgrade

archive: dist
	cd build && git archive --prefix "bubaak/" -o ../bubaak.zip -9 --format zip HEAD

