# Bubaak

Bubaak is a set of scripts that runs program verifiers in parallel and
exchanges verification artifacts between them at runtime. Rign now, the default
verifiers are BubaaK-KLEE (a fork of KLEE) and SlowBeast. To exchange the data,
the tools must be manually modified to share partial results of analyses.

         ????
        ? 00 ?
      >-|  O |-<



# Building

```
git clone https://gitlab.com/mchalupa/bubaak
cd bubaak
git submodule update --init
make setup
```

The command `make setup` tries to get and build dependencies.
If the command for some reason fails, you can try to obtain and build
the dependencies manually:

```
# apt install cmake clang llvm pip
### Building BubaaK-LEE
# apt install libsqlite3-dev libz3-dev zlib1g
# pip install lit # to run tests with KLEE
cd klee; mkdir build && cd build
# tcmalloc: either install tcmalloc or use
# -DENABLE_TCMALLOC=off in the following command
cmake .. -DCMAKE_INSTALL_PREFIX=$(pwd)/install
make -j4
make install

### Building slowbeast package
cd ../..
cd slowbeast
pip install z3-solver
git clone https://github.com/mchalupa/llvmlite
cd llvmlite && python3 ./setup.py build && cd ..

# that's it for slowbeast, but if you want to build a package:
pip install pyinstaller
pyinstaller sb
```

Once the project is all setup, you can build a zip with `make archive`.

## Author & Support
Marek Chalupa, mchqwerty@gmail.com
